#!/bin/bash

BOLD=$(tput bold)
STD=$(tput sgr0)

# Define Variables

DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

chmod u+x ./scripts/*
cd $SCRIPT_DIR


while :
do
	clear
	if [[ $CONFIRM = "ACAB" ]]; then
    		break
	fi

	echo
	echo "    ##################################"
	echo "   #### Welcome to Element Desktop ####"
	echo " ######      Tails Installer       ######"
	echo "    ##################################"
	echo
	echo "! An administrator password is necessary !"
	echo "!   for install and update procedures.   !"	
	echo
	echo -e "${BOLD}Would you like to :${STD}"
	echo
	echo -e "Install element-desktop ? "
	echo -e "Then write ${BOLD}install${STD} and press enter"
	echo
	echo -e "Save current configuration ?"
	echo -e "Then write ${BOLD}save${STD} and press enter" 
	echo
	echo -e "Update element-desktop ?"
	echo -e "Then write ${BOLD}update${STD} and press enter"
	echo
	echo -e "Uninstall element-desktop ? "
	echo -e "Then write ${BOLD}uninstall${STD} and press enter"
	echo
	read -p "Your choice : " -r CHOICE
	echo
	echo
	echo
	echo -e "${BOLD}If it's the good choice, then write ACAB and press enter.${STD}"
	echo -e "${BOLD}Anything else will bring you back to the choice menu.${STD}"
	read -p "" -r CONFIRM
	echo
	echo
done


if [[ $CHOICE = "install" ]]
	then 	./scripts/connectivity-check.sh
		./scripts/element-install.sh $SCRIPT_DIR $DOTFILES

	echo
	echo "${BOLD}Wonderful !${STD}"
	echo "${BOLD}The install is finished !${STD}"
	echo "${BOLD}:)${STD}"

elif [[ $CHOICE = "save" ]]
	then ./scripts/element-save.sh
	echo
	echo "${BOLD}Configuration saving is completed.${STD}"
	echo "${BOLD}Well done comrade.${STD}"	


elif [[ $CHOICE = "update" ]]
	then 	./scripts/connectivity-check.sh
		./scripts/element-update.sh $SCRIPT_DIR $DOTFILES
	echo
	echo "${BOLD}The update is total.${STD}"	
	echo "${BOLD}No one can stop us.${STD}"

elif [[ $CHOICE = "uninstall" ]]
	then ./scripts/element-uninstall.sh $SCRIPT_DIR $DOTFILES
	echo
	echo "${BOLD}Complete removal is done.${STD}"
	echo "${BOLD}No traces, no problems.${STD}"

else 
	echo -e "${BOLD}Your choice wasn't understood, nothing was touched."
	echo -e "${BOLD}You should launch this script again.${STD}"
fi

echo
echo "${BOLD}Bye bye.${STD}"
