# Objectifs :

- Installer le client matrix `element-desktop` dans Tails.
- Conserver la configuration dans la mémoire persistente.
- Rendre possible la mise à jour de `element-desktop`.
- Rendre possible la desinstallation de `element-desktop`.

# Prérequis :

- Etre connécté à internet
- Pouvoir exécuter des commandes `sudo`. Sur Tails, il faut pour cela définir un mot de passe Administrateur au démarrage.

# Procédure :

- Télécharger le dossier (petite icone à gauche de `clone` en haut de cette page) et le dézipper. 
- Entrer dans le dossier et y ouvrir un terminal (clic droit, ouvrir dans un terminal).
- Ecrire `./element-desktop-configure.sh` et se laisser guider.

# Le dossier contient :

- Un dossier `data/dynamic` contenant les paquets téléchargés pendant l'installation.
- Un dossier `data/static` contenant certains fichiers qui seront copiés durant l'installation.
- Un dossier `scripts` contenant les scripts appelés par `element-desktop-configure.sh`.
- Un dossier `scripts/actions` contenant les actions découpées en scripts le plus basiques possibles.
