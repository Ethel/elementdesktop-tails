#!/bin/bash

# Define Variables
BOLD=$(tput bold)
STD=$(tput sgr0)
DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"

echo
echo -e "${BOLD}Installation of element-desktop${STD}"

# Create filesystem
./scripts/actions/arborescence.sh

# Install sources
./scripts/actions/install-sources.sh

# Download element-desktop
./scripts/actions/download.sh

# Install element-desktop
./scripts/actions/extract.sh

# Remove deb package
rm -rvf data/dynamic/element-desktop/*.deb


## Creating persistent configuration
echo
echo "${BOLD}The installation is almost completed.${STD}"
echo "${BOLD}Would you like to launch element-desktop now and configure it ?${STD}"
echo "If you decide to configure element-desktop later,"
echo "once it is done, do not forget to launch this installer again"
echo "and select the ${BOLD}save${STD} option."
echo "Elsif your configuration will not be persistent."
echo
read -p "${BOLD}Configure element-desktop now (Y/n) ? ${STD}" -n 1 -r FIRSTCONF

if [[ ! $FIRSTCONF =~ ^[Nn]$ ]]
	then
		clear
		echo
		echo -e "${BOLD}Launching element-desktop${STD}"
		nohup /live/persistence/TailsData_unlocked/dotfiles/Applications/element-desktop/opt/Element/element-desktop --no-sandbox %U --proxy-server=socks5://127.0.0.1:9050 >/dev/null 2>&1 &
		echo "Element-desktop will now show."
		echo "You can set up your credentials and configure the client."
		echo "Once you finished configuring element-desktop, exit it."
		echo "Then come back to this window and press any key"
		echo "It will save your configuration and exit this script."
		echo
		read -p "${BOLD}Press any key to continue${STD}" -n 1 -r CONTINUEYN
		./scripts/element-save.sh
fi
