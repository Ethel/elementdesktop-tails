#!/bin/bash

# Define variables
BOLD=$(tput bold)
STD=$(tput sgr0)

echo
echo -e "${BOLD}Uninstalling element-desktop :${STD}"

# Uninstallation
./scripts/actions/clean.sh
./scripts/actions/remove.sh
