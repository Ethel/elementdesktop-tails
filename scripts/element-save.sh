#!/bin/bash

# Define Variables
BOLD=$(tput bold)
STD=$(tput sgr0)
DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"

echo
echo -e "${BOLD}Saving element-desktop configuration.${STD}"

# Save config
mkdir -pv ${DOTFILES}/.config/Element/
rm -rf ${DOTFILES}/.config/Element/*
cp -rv /home/amnesia/.config/Element/* ${DOTFILES}/.config/Element/


