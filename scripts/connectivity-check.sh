#!/usr/bin/env bash

BOLD=$(tput bold)
STD=$(tput sgr0)

# Test connectivity and continue if connected
while :
do
	echo "${BOLD}Connection test...${STD}"
	curl check.torproject.org 1>/dev/null 2>&1 
	if [ $? -eq 0 ]
	then 
		echo "${BOLD}Internet is reachable, let's go.${STD}"
		break
	else 
		echo "${RED}To continue, you need to be connected to internet. ${BOLD}You can do it while keeping this window openned.${STD}"
		sleep 2
fi
done
