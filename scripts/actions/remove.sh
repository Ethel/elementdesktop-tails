#!/bin/bash

# Define Variables
BOLD=$(tput bold)
STD=$(tput sgr0)
DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"

# Remove element-desktop
echo
echo -e "${BOLD}Removing application...${STD}"
rm -rvf ${DOTFILES}/Applications/element-desktop
rm -rvf /home/amnesia/Applications/element-desktop
echo
echo -e "${BOLD}Removing tails-launcher...${STD}"
rm -rvf ${DOTFILES}/Applications/element-scripts
rm -rvf /home/amnesia/Applications/element-scripts
rm -vf ${DOTFILES}/.local/share/applications/Element.desktop
rm -vf /home/amnesia/.local/share/applications/Element.desktop


