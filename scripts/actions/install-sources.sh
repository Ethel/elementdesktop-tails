#!/bin/bash

# Define Variables
BOLD=$(tput bold)
STD=$(tput sgr0)
DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"

# Import keys
echo
echo -e "${BOLD}Get element-desktop repository pgp key${STD}"
while true;do
	wget -T 15 -c --directory-prefix=data/dynamic/keyring/ https://packages.element.io/debian/element-io-archive-keyring.gpg && break
done

# Install sources
echo
echo -e "${BOLD}Enter sudo password to continue${STD}"
sudo ./scripts/actions/install-sources-sudo.sh
