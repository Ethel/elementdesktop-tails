#!/bin/bash

# Define Variables
BOLD=$(tput bold)
STD=$(tput sgr0)
DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"

# Extract element-desktop
echo
echo -e "${BOLD}Extracting element-desktop.${STD}"
cd data/dynamic/element-desktop/
dpkg-deb -xv $(ls element-desktop*.deb) $DOTFILES/Applications/element-desktop/
while : ; do
	if [ $? -eq 0 ]; then
		echo "${BOLD}Bravo ! element-desktop a été installé.${STD}"
		break
	fi
	echo -e "${BOLD}Echec de l'installation de element-desktop.${STD}"
	echo -e "${BOLD}          Devil is your last hope.         ${STD}"
	echo -e "${BOLD}                    666                    ${STD}"
	exit
done
cd ../../../

# Copy element-desktop launcher
echo
echo -e "${BOLD}Copying element-desktop tails launcher.${STD}"
cp -vf data/static/launcher/Element.desktop $DOTFILES/.local/share/applications/
cp -vf data/static/launcher/Element.desktop /home/amnesia/.local/share/applications/
cp -vf data/static/scripts/element-desktop-startup.sh $DOTFILES/Applications/element-scripts/





