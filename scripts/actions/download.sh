#!/bin/bash

# Define Variables
DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"
BOLD=$(tput bold)
STD=$(tput sgr0)

# Download element-desktop
echo
echo -e "${BOLD}Downloading element-desktop.${STD}"
cd data/dynamic/element-desktop/
while true;do
	apt download element-desktop && break
done
cd ../../../

# state if an old version is here and remove it

