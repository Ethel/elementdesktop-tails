#!/bin/bash

# Define variables
BOLD=$(tput bold)
STD=$(tput sgr0)

# Remove precedent sources
rm /usr/share/keyrings/element-io-archive-keyring.gpg
rm /etc/apt/sources.list.d/element-io.list

# Enable element-desktop sources
echo
echo -e "${BOLD}Installing element-desktop sources.${STD} (The sources are not stored in persistence)"
cat data/dynamic/keyring/element-io-archive-keyring.gpg | tee -a /usr/share/keyrings/element-io-archive-keyring.gpg > /dev/null
echo "Add the element-desktop repo to APT"
echo 'deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] tor+https://packages.element.io/debian/ default main' | sudo tee -a /etc/apt/sources.list.d/element-io.list
chown -v root:root /etc/apt/sources.list.d/element-io.list
chmod -v 644 /etc/apt/sources.list.d/element-io.list

# Update and dependencies installation
echo
echo -e "${BOLD}Updating sources and installing dependencies.${STD}"
apt update
apt install -y apt-transport-https

