#!/bin/bash

# Define Variables
BOLD=$(tput bold)
STD=$(tput sgr0)
DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"

# Create filesystem
echo
echo -e "${BOLD}Creating filesystem${STD}"
mkdir -p data/dynamic/keyring/
mkdir -p data/dynamic/element-desktop/
mkdir -pv $DOTFILES/Applications/element-desktop/
mkdir -pv $DOTFILES/Applications/element-scripts/
mkdir -pv $DOTFILES/.config/Element/
