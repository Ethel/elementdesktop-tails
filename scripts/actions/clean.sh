#!/bin/bash

# Define Variables
BOLD=$(tput bold)
STD=$(tput sgr0)
DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"

# Remove configuration files
echo
echo -e "${BOLD}Removing configuration...${STD}"
rm -rvf $DOTFILES/.config/Element
rm -rvf /home/amnesia/.config/Element
