#!/bin/bash

# Define Variables
BOLD=$(tput bold)
STD=$(tput sgr0)
DOTFILES="/live/persistence/TailsData_unlocked/dotfiles"

echo
echo -e "${BOLD}Updating element-desktop.${STD}"

# Update installer
echo
echo -e "${BOLD}Updating installer.${STD}"
# git pull

# Install sources
./scripts/actions/install-sources.sh

# Download element-desktop
./scripts/actions/download.sh

# Remove current version
./scripts/actions/remove.sh

# Create filesystem
./scripts/actions/arborescence.sh

# Install element-desktop
./scripts/actions/extract.sh

# Remove deb package
rm -rvf data/dynamic/element-desktop/*.deb
